﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="JScript.aspx.cs" Inherits="Assignment2b_N00669744.WebForm2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainConcept" runat="server">
    <h1>Javascript/CSS: Review</h1>
    <p>As a review for Javascript Objects (with some CSS), I started creating a simple BlackJack 
        game.</p>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="CodeOutput" runat="server">
    <div class="col-lg-12">
        <h2>HTML:</h2>
        <code>
	       <img src="htmlGame.PNG" alt="HTML from BlackJack game" title="HTML Code" />
        </code>
    </div>
    <div class="col-lg-12">
    <h2>Javascript: </h2>
        <code>
            <img src="jsGame.PNG" alt="Javascript from Blackjack game" title="JS Code" />
        </code>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="LearningOutcomes" runat="server">
    <h2>Code Explanation: </h2>
    <p>The code above is simple, as each card is represented by an Object. The object contains its standard
        value, as well as its value in BlackJack. Also, the objects is then stored in a an array, which will
        be the deck and how I will be accessing each of the card objects.
    </p>
    <p>Next, a random card generator function is created to retrieve a value from 0-51, where each number
        corresponds to a card in the Deck array. The function will return a random card object, which will be 
        used later on.
    </p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Future" runat="server">
    <div class="col-lg-6">
    <h2>Future Outcomes:</h2>
    <p>In the future, I wish to add an image to each of the card objects, as well as start displaying the
        cards onto the web page. The game is a work in progress for when I gain more knowledge 
        in using Javascript.
    </p>
        </div>
    
</asp:Content>

 <asp:Content ID="Content5" ContentPlaceHolderID="usefulLinks" runat="server">
     <div class="col-lg-6">
        <h2>Useful Links: </h2>
        <ul>
            <li><a href="https://www.w3schools.com/jsref/event_onclick.asp">Button Events</a></li>
            <li><a href="https://www.w3schools.com/jsref/dom_obj_image.asp">DOM Images</a></li>
        </ul>
     </div>
  </asp:Content>