﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Assignment2b_N00669744._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainConcept" runat="server">

    <div class="row">
        <div class="">
            <h1>My Midterm Review Page! Now with codeblocks!</h1>
        </div>

        <ul>
            <li><a href="CSS.aspx">CSS</a></li>
            <li><a href="JScript.aspx">Javascript</a></li>
            <li><a href="SQL.aspx">SQL</a></li>
        </ul>
    </div>

</asp:Content>
