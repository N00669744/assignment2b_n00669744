﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SQL.aspx.cs" Inherits="Assignment2b_N00669744.WebForm3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainConcept" runat="server">
    <h1>SQL: Code Explanation</h1>
    <p>In HTTP 5105: Database Design & Development, we study how to create queries to retrieve 
        certain data based on the required specifications.
    </p>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="CodeOutput" runat="server">
    
        <h2>The Code:</h2>
        <code>
            <img src="codeExplain.PNG" alt="SQL code from Christine's database class" />
        </code>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="LearningOutcomes" runat="server">

    <h2>Code Explanation: </h2>
    <div class="col-lg-6 col-md-12">
        <p>1. The code (without looking at the question) is asking for the clients first name, the sum
            of all the clients invoices, as well as finds the minimum invoice due date, in other words
            that client's earliest invoice each client has.
        </p>

        <p>2. An inner join is used as we are only looking at clients WITH invoices, and is performed 
            using each table's primary key.
        </p>
    </div>
    <div class="col-lg-6 col-md-12">
        <p>3. Aggregate functions are used for summation and finding the minimum; therefore, a 'having' 
            statement is required, as well as a 'group by' statment for the non-aggregate functions and
            primary key.
        </p>
        <p>4. To restrict the client information to only those with invoice sums greater than 1500, the
            'having' function is applied to the invoice sums. And lastly, the data is assorted in 
            descending order, as in the largest invoice sums are at the top.
        </p>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Future" runat="server">
    <h2>Future Outcome:</h2>
    <p>In the future I want to be able to create/manage my own databases filled with information
        from my own designed webpage.
    </p>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="usefulLinks" runat="server">
    <div>
        <p>The previous code was taken from Assignment 3, of HTTP 5105, and was made by and posted
            to Blackboard by Christine Bittle.
        </p>

        <h2>Useful Links: </h2>
        <ul>
            <li><a href="https://www.w3schools.com/sql/sql_having.asp">SQL Having clause</a></li>
            <li><a href="https://www.w3schools.com/sql/sql_create_db.asp">SQL Create Database</a></li>
        </ul>
     </div>
</asp:Content>
