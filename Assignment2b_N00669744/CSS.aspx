﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CSS.aspx.cs" Inherits="Assignment2b_N00669744.WebForm1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainConcept" runat="server">
     <div class="col-lg-12">
    <h1>HTTP 5104: Digital Design</h1>
       The most tricky part of CSS is understanding the positioning of elements, especially how to use box-sizing in conjuction
            with floats to create responsive layouts. To get the desired layout the width percentages should be calculated such that it equals
            a total of 100%, as to fill its containing wrapper. 
        </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="CodeOutput" runat="server">
    <div class="col-lg-8 col-md-8 col-sm-6">
        <h2>Example Code:</h2>
            <div>
            html{box-sizing: border-box;font-size: 16px;}
             </div>
            *, *::after, *::before{box-sizing: inherit;}
             <div>
            #page-wrapper{ max-width: 980px;margin: 0 auto;}
            </div>
             <div>
            #form{
                    width: 65.45%;
                    background: #ffdb91;
                    padding: 1em;
                    border-radius: 10px;
                    float: left;
                    margin-right: 2.5%;
                    font-size: 1.2em;
                }
            </div>
    
            <div>
             #contact {
                width: 32.05%;
                float: right;
                border: 5px solid #29a0fc;
                border-radius: 10px;
                padding: 1.5em;
                }
            </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="LearningOutcomes" runat="server">
    <div class="col-lg-4 col-md-4 col-sm-6">
        <h2>Code Explanation: </h2>
        <p>Setting the entire HTML page to border-box, and having it being inherited by all the elements
            allows us to manipulate the elements more easily. The page-wrapper provides a restriction to
            the assigned class elements width, as the containing elements will not exceed the max-width
            set. 
        </p>
        <p>Next, the widths of the page containers are set to have 1/3 and 2/3 split, and each respective
            column is floated to the desired direction within its container. The percentages are calculated
            based on the desired split and includes the margin percentages, as well.
        </p>
     </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Future" runat="server">
    <div class="col-lg-6">
        <h2>Future Learning: </h2>
        <p>In the future, I wish to learn more about the other methods of positioning, such as, flexbox, or
            maybe using a framework like Boostrap to make certain elements of CSS much easier.
        </p>
     </div>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="usefulLinks" runat="server">
     <div>
        <h2>Useful Links: </h2>
        <ul>
            <li><a href="https://www.w3schools.com/css/css3_box-sizing.asp">Box Sizing</a></li>
            <li><a href="https://www.w3schools.com/bootstrap/bootstrap_grid_basic.asp">Bootstrap</a></li>
        </ul>
     </div>
  </asp:Content>
